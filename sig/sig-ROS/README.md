# openEuler ROS SIG

- 在openEuler社区中添加对ROS的支持
- 根据openEuler迭代版本，持续完成ROS中各个组件向openEuler的移植，并提供ROS在openEuler上的使用文档
- 及时响应用户反馈，解决相关问题


# 组织会议

- 公开的会议时间：北京时间，周四 上午，10点-11点


# 成员

### Maintainer列表

- 吴伟[@wuwei_plct](https://gitee.com/wuwei_plct)
- 安传旭[@anchuanxu](https://gitee.com/anchuanxu)
- 王晓云[@xiao_yun_wang](https://gitee.com/xiao_yun_wang)

### Committer列表

- 吴伟[@wuwei_plct](https://gitee.com/wuwei_plct)
- 安传旭[@anchuanxu](https://gitee.com/anchuanxu)
- 王晓云[@xiao_yun_wang](https://gitee.com/xiao_yun_wang)


# 联系方式

- [邮件列表](dev@openeuler.org)
- [IRC频道](#openeuler-ros)
- [IRC公开会议](#openeuler-meeting)


# 项目清单

项目名称：ROS

repository地址：

- https://gitee.com/openeuler/ros
- https://gitee.com/src-openeuler/catkin
- https://gitee.com/src-openeuler/class_loader
- https://gitee.com/src-openeuler/cmake_modules
- https://gitee.com/src-openeuler/cpp_common
- https://gitee.com/src-openeuler/gencpp
- https://gitee.com/src-openeuler/geneus
- https://gitee.com/src-openeuler/genlisp
- https://gitee.com/src-openeuler/genpy
- https://gitee.com/src-openeuler/gennodejs
- https://gitee.com/src-openeuler/genmsg
- https://gitee.com/src-openeuler/std_msgs
- https://gitee.com/src-openeuler/message_generation
- https://gitee.com/src-openeuler/ros_environment
- https://gitee.com/src-openeuler/message_runtime
- https://gitee.com/src-openeuler/roscpp_core
- https://gitee.com/src-openeuler/ros
- https://gitee.com/src-openeuler/rosconsole

